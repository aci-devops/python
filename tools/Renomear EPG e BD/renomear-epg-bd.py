import requests
import json
import time
import getpass
import urllib3

#Testado usando no Sandbox Reserved#
#https://devnetsandbox.cisco.com/RM/Diagram/Index/4eaa9878-3e74-4105-b26a-bd83eeaa6cd9?diagramType=Topology

username = input("Digite o username do APIC: ")
password = getpass.getpass()
apic = '10.10.20.14'
protocolo = 'https://'
preambulo = protocolo + apic
tenant = input("Digite o nome do Tenant: ")
application_profile = input("Digite o nome do Application Profile: ")
bridge_domain_antiga = ''
bridge_domain_nova = ''
epg_antigo = ''
epg_novo = ''

# Disabilitando os aviso de SSL
urllib3.disable_warnings()


#Função login
def login():
    url = preambulo + '/api/aaaLogin.json'
    payload = {'aaaUser':{'attributes':{'name':username,'pwd':password}}}
    session = requests.Session()
    response = session.post(url, json=payload, verify=False)
    return session

#Função criar snapshot
def criar_snapshot(session, apic, bridge_domain_antiga, epg_antigo):
    snapshot_description = '"Renomeando ' + epg_antigo + '/' + bridge_domain_antiga + '"'
    payload_string = '{"configExportP":{"attributes":{"dn":"uni/fabric/configexp-defaultOneTime","adminSt":"triggered","descr":' + snapshot_description + '}}}'
    url = preambulo + '/api/mo.json'
    payload = json.loads(payload_string)
    response = session.post(url, json=payload, verify=False)
    print('Snapshot criado com a descrição: ' + snapshot_description + '\n')

#Função para pegar os epgs
def listar_epgs(session, apic):
    url = preambulo + '/api/node/mo/uni/tn-' + tenant + '/ap-' + application_profile + '.json?query-target=children&target-subtree-class=fvAEPg&rsp-subtree=full&rsp-prop-include=config-only'
    response = session.get(url, verify=False)
    return json.loads(response.text)

#Função para pegar os bds
def listar_bds(session, apic):
    url = preambulo + '/api/node/mo/uni/tn-' + tenant + '.json?query-target=children&target-subtree-class=fvBD&rsp-subtree=full&rsp-prop-include=config-only'
    response = session.get(url, verify=False)
    return json.loads(response.text)

#Função para deletar os bds antigos
def deletar_bd_antigo(session, apic, tenant, bridge_domain_antiga):
    url = preambulo + '/api/node/mo/uni/tn-' + tenant + '/BD-' + bridge_domain_antiga + '.json'
    payload_string = '{"fvBD":{"attributes":{"dn":"uni/tn-' + tenant + '/BD-' + bridge_domain_antiga + '","status":"deleted"}}}'
    payload = json.loads(payload_string)
    response = session.post(url, json=payload, verify=False)
    print('Renomeando a bridge domain ' + bridge_domain_antiga + ' para ' + bridge_domain_nova + '\n')

#Função para deletar os epgs antigos
def deletar_epg_antigo(session, apic, tenant, application_profile, epg_antigo):
    url = preambulo + '/api/node/mo/uni/tn-' + tenant + '/ap-' + application_profile + '/epg-' + epg_antigo + '.json'
    payload_string = '{"fvAEPg":{"attributes":{"dn":"uni/tn-' + tenant + '/ap-' + application_profile + '/epg-' + epg_antigo + '","status":"deleted"},"children":[]}}'
    payload = json.loads(payload_string)
    response = session.post(url, json=payload, verify=False)
    print('Renomeando o EPG ' + epg_antigo + ' para ' + epg_novo + '\n')

#Função para recirar os BDs 
def recriando_novo_bd(session, apic, tenant, bds, bridge_domain_antiga, bridge_domain_nova):
    for bd in bds['imdata']:
        if bd['fvBD']['attributes']['name'] == bridge_domain_antiga:
            bridge_domain_antiga_json = bd
    bridge_domain_antiga_json_string = json.dumps(bridge_domain_antiga_json, separators=(',',':'))
    bridge_domain_nova_json_string = bridge_domain_antiga_json_string.replace(bridge_domain_antiga, bridge_domain_nova)
    payload = json.loads(bridge_domain_nova_json_string)
    url = preambulo + '/api/node/mo/uni/tn-' + tenant +'/BD-' + bridge_domain_nova + '.json'
    response = session.post(url, json=payload, verify=False)
    #print('O nome da nova bridge domain será: ' + bridge_domain_nova + '\n')
    for child in bridge_domain_antiga_json['fvBD']['children']:
        if 'fvSubnet' in child:
            print('Obs.: A bridge domain ' + bridge_domain_nova + ' manterá a mesma subnet ' + child['fvSubnet']['attributes']['ip'])

#Função para recirar os EPGs
def recriar_epg(session, apic, tenant, application_profile, epgs, epg_antigo, epg_novo, bridge_domain_antiga, bridge_domain_nova):
    for epg in epgs['imdata']:
        if epg['fvAEPg']['attributes']['name'] == epg_antigo:
            epg_antigo_json = epg
    epg_antigo_json_string = json.dumps(epg_antigo_json, separators=(',',':'))
    epg_novo_json_string = epg_antigo_json_string.replace(epg_antigo, epg_novo)
    epg_novo_json_string = epg_novo_json_string.replace(bridge_domain_antiga, bridge_domain_nova)
    payload = json.loads(epg_novo_json_string)
    url = preambulo + '/api/node/mo/uni/tn-' + tenant + '/ap-' + application_profile + '/epg-' + epg_novo +'.json'
    response = session.post(url, json=payload, verify=False)
    #print('O nome do novo EPG será: ' + epg_novo)

#Função para referenciar o BD com EPG
def referencia(bds, epgs, bridge_domain_antiga, epg_antigo):
    for epg in epgs['imdata']:
        if epg['fvAEPg']['attributes']['name'] == epg_antigo:
            filtrado_epg_antigo = epg
            break
        else:
            filtrado_epg_antigo = None
    for children in filtrado_epg_antigo['fvAEPg']['children']:
        try:
            if children['fvRsBd']['attributes']['tnFvBDName'] == bridge_domain_antiga:
                print('Bridge domain encontrado na configuração do EPG... Continuando..' + '\n')
                continue
            else:
                print('Bridge domain não encontrada na configuração do EPG...  Verifique o nome do EPG e da BD.' + '\n')
                exit()
        except KeyError:
            continue

#Função para checar BD antigo com EPG antigo
def bd_antigo_no_epg_antigo(epgs, epg_antigo):
    for epg in epgs['imdata']:
        if epg['fvAEPg']['attributes']['name'] == epg_antigo:
            for child in epg['fvAEPg']['children']:
                if 'fvRsBd' in child:
                    bridge_domain_antiga = child['fvRsBd']['attributes']['tnFvBDName']
                else:
                    pass
        else:
            pass
    time.sleep(3)
    print('\n')
    print("                  Atenção" +'\n')
    time.sleep(1)    
    print('*** O EPG ' + epg_antigo + ' está associado a bridge-domain ' + bridge_domain_antiga + ' ***' + '\n')
    return bridge_domain_antiga

#Função para pegar BD antigo 
def listar_bd_antigo(lista_bd):
    while True:
        bridge_domain_antiga = input('Bridge Domain antiga: ' )
        if bridge_domain_antiga not in lista_bd:
            print('Não é uma BD válida.  Selecione da lista sem aspas.' + '\n')
            print('BDs: ' + str(lista_bd) + '\n')
        elif bridge_domain_antiga in lista_bd:
            return bridge_domain_antiga

#Função para pegar EPG antigo 
def listar_epg_antigo(lista_epg):
    while True:
        epg_antigo = input('Digite qual EPG gostaria de renomear: ')
        if epg_antigo not in lista_epg:
            print('Não é um EPG válido. Selecione da lista sem aspas.' + '\n')
            print('Lista dos EPGs encontrados no tenant ' + tenant + ':' + str(lista_epg) + '\n')
        elif epg_antigo in epg_antigo:
            return epg_antigo

#Função para validar o novo BD
def validar_novo_bd():
    while True:
        bridge_domain_nova = input('Qual seria o novo nome da Bridge-domain: ')
        print('\n')
        if bridge_domain_nova == '':
            print('Atenção! Forneça um nome de BD válido.' + '\n')
        else:
            break
    return bridge_domain_nova

#Função para validar o novo EPG
def validar_novo_epg():
    while True:
        epg_novo = input('Qual seria o novo nome para o EPG: ')
        print('\n')
        if epg_novo == '':
            print('Digite um EPG válido' + '\n')
        else:
            break
    return epg_novo

#Função para listar os EPGs
def lista_epgs(epgs):
    lista_epg = []
    for epg in epgs['imdata']:
        lista_epg.append(epg['fvAEPg']['attributes']['name'])
    print('\n')    
    print('Lista dos EPGs encontrados no tenant ' + tenant + ': ' + str(lista_epg) + '\n')
    return lista_epg

#Função para listar os BDs
def lista_bds(bds):
    lista_bd = []
    for bd in bds['imdata']:
        lista_bd.append(bd['fvBD']['attributes']['name'])
    print('BDs: ' + str(lista_bd) + '\n')
    return lista_bd

#Chamando a main
if __name__ == "__main__":
    session=login()
    bds = listar_bds(session=session, apic=apic)
    epgs = listar_epgs(session=session, apic=apic)
    lista_epg = lista_epgs(epgs=epgs)
    epg_antigo = listar_epg_antigo(lista_epg=lista_epg)
    bridge_domain_antiga = bd_antigo_no_epg_antigo(epgs=epgs, epg_antigo=epg_antigo)
    epg_novo = validar_novo_epg()
    bridge_domain_nova = validar_novo_bd()
    time.sleep(2)
    criar_snapshot(session=session, apic=apic, bridge_domain_antiga=bridge_domain_antiga, epg_antigo=epg_antigo)
    time.sleep(3)
    deletar_bd_antigo(session=session, apic=apic, tenant=tenant, bridge_domain_antiga=bridge_domain_antiga)
    time.sleep(3)
    deletar_epg_antigo(session=session, apic=apic, tenant=tenant, application_profile=application_profile, epg_antigo=epg_antigo)
    time.sleep(3)
    recriar_epg(session=session, apic=apic, tenant=tenant, application_profile=application_profile, epgs=epgs, epg_antigo=epg_antigo, epg_novo=epg_novo, bridge_domain_antiga=bridge_domain_antiga, bridge_domain_nova=bridge_domain_nova)
    time.sleep(3)
    recriando_novo_bd(session=session, apic=apic, tenant=tenant, bds=bds, bridge_domain_antiga=bridge_domain_antiga, bridge_domain_nova=bridge_domain_nova)
    

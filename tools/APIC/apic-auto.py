import pexpect
import sys
import time
import getpass

#Testado no CPOC da Cisco 
apic_cimc = ["192.168.1.1",
             "192.168.1.2",
             "192.168.1.3",
             "192.168.1.4"]
#Trocar os IP´s
apic_gui = ["172.16.100.1/24",
            "172.16.100.2/24",
            "172.16.100.3/24",
            "172.16.100.4/24"]
senha_inicial = ""
apic_id = ""

####################### Execução ######################################
print("-----------  APIC BringUp by Freitas  -----------\n")


while not senha_inicial:
    senha_inicial = getpass.getpass(
        "Informe a senha da CIMC: ")

while not apic_id:
    try:
        apic_id = int(
            input("Informe o número do APIC quer iremos configurar [1-4]: "))
    except ValueError:
        print("Erro! Esse número não é válido!")
    if not (1 <= apic_id <= 4):
        print("Erro! Digite um número válido!")
        apic_id = ""

####################### Configuração ##################################

def configurando_apic(apic_id, apic_cimc, apic_gui, senha_inicial):
    #Entrando via SSH no servidor
    ssh_cmd = f"ssh admin@"+apic_cimc
    prompt = pexpect.spawn(ssh_cmd)
    prompt.logfile = sys.stdout.buffer
    prompt.expect("admin@.* password:")
    time.sleep(1)
    prompt.sendline(senha_inicial)
    prompt.expect(".*#")
    
    # Connectando ao Host
    prompt.sendline("connect host")
    time.sleep(2)
    prompt.sendline("\n")
    time.sleep(5)
    #Voltando ao prompt inicial para configuração
    prompt.sendcontrol("d")

    # Configurando o  APIC
    prompt.expect("Enter the fabric name .*:")
    time.sleep(1)
    prompt.sendline("Fabric1")
    prompt.expect("Enter the fabric ID .*:")
    time.sleep(1)
    prompt.sendline("1")
    prompt.expect("Enter the number of active controllers .*:")
    time.sleep(1)
    prompt.sendline("3")
    prompt.expect("Is this .*:")
    time.sleep(1)
    if apic_id != 4:   
        prompt.sendline("NO")
    else:  
        prompt.sendline("YES")
        
    if apic_id != 4:  
        prompt.expect("Enter the controller ID .*:")
    else:
        prompt.expect("Enter the .* controller ID .*:")
    time.sleep(1)
    prompt.sendline(str(apic_id))

    prompt.expect("Standalone APIC Cluster .*:")
    time.sleep(1)
    prompt.sendline("no")

    prompt.expect("Enter the POD ID .*")
    #Se for mpod
    time.sleep(1)
    if apic_id <= 2:
        prompt.sendline("1")
    else:
        prompt.sendline("2")
        
    prompt.expect("Enter the controller name .*:")
    time.sleep(1)
    prompt.sendline("APIC-0"+str(apic_id))

    prompt.expect("Enter address pool for TEP addresses .*:")
    time.sleep(1)
    prompt.sendline("10.0.0.0/16") #recomendado um /16 - alterar conforme necessidade.

    prompt.expect("Enter the VLAN ID for infra network .*:")
    time.sleep(1)
    prompt.sendline("3697") #alterar conforme necessidade.

    if apic_id == 1:
        prompt.expect("Enter address pool for BD multicast addresses .*:")
        time.sleep(1)
        prompt.sendline("225.0.0.0/15")

    prompt.expect("Enable IPv6 for .*:")
    time.sleep(1)
    prompt.sendline("N")

    prompt.expect("Enter the IPv4 address .*:")
    time.sleep(1)
    prompt.sendline(apic_gui)

    prompt.expect("Enter the IPv4 address of the default gateway .*: ")
    time.sleep(1)
    prompt.sendline("2.2.2.254") #alterar conforme necessidade.

    prompt.expect("Enter the interface speed/duplex mode .*:")
    time.sleep(1)
    prompt.sendline("auto")

    if apic_id == 1: 
        prompt.expect("Enable strong .*:")
        time.sleep(1)
        prompt.sendline("N")

        prompt.expect("Enter the password for .*:")
        time.sleep(1)
        prompt.sendline(senha_inicial)

        prompt.expect("Reenter the password for admin:")
        time.sleep(1)
        prompt.sendline(senha_inicial)

    prompt.expect("Would you like to edit the .*:")
    time.sleep(1)
    prompt.sendline("n")

    print("#------ APIC-0"+str(apic_id)+" Configuração Completa! ------")


if __name__ == "__main__":
    configurando_apic(apic_id, apic_cimc[apic_id-1], apic_gui[apic_id-1], senha_inicial)

# Automatizando a configuração do Cisco APIC


## Sobre o script

Este script em python tem a função de automatizar o processo de construção do APIC ACI.

## Como utilizar?

```
python3 apic-auto.py
```

## Autor
Freitas®

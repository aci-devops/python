import requests
import json
import time
import getpass
import urllib3
import re
import csv

#Testado usando no Sandbox Reserved#
#https://devnetsandbox.cisco.com/RM/Diagram/Index/4eaa9878-3e74-4105-b26a-bd83eeaa6cd9?diagramType=Topology



username = input("Digite o username do APIC: ")
password = getpass.getpass()
apic = '10.10.20.14'



# Disabilitando os avisos de SSL
urllib3.disable_warnings()


#Funçao do Login
def login():
    
    url = 'https://' + apic + '/api/aaaLogin.json'
    payload = {'aaaUser':{'attributes':{'name':username,'pwd':password}}}
    session = requests.Session()
    response = session.post(url, json=payload, verify=False)
    return session


#Função para validar o formato
def formato(interface_id):

    pattern = re.compile('^\d+\/\d+$')
    match = pattern.match(interface_id)
    if not match:
        print("Formato Inválido")
        quit()
    else:
        pass
    interface_split = []
    interface_split.append(interface_id.split('/'))
    interface_underscore = str(interface_split[0][0]) + '_' + str(interface_split[0][1])
    return interface_underscore


#Funçao para setar a descrição na interface
def set_interface_description(session, apic, node_id, interface_id, interface_description):

    interface_underscore = formato(interface_id)
    url = 'https://' + apic + '/api/node/mo/uni/infra/hpaths-' + node_id + '_eth' + interface_underscore + '.json'
    json = {"infraHPathS":{"attributes":{"rn":"hpaths-" + str(node_id) + "_eth" + interface_underscore + "","dn":"uni/infra/hpaths-" + str(node_id) + "_eth" + interface_underscore + "","descr":"" + interface_description + "","name":"" + str(node_id) + "_eth" + interface_underscore + ""},"children":[{"infraRsHPathAtt":{"attributes":{"dn":"uni/infra/hpaths-" + str(node_id) + "_eth" + interface_underscore + "/rsHPathAtt-[topology/pod-1/paths-" + node_id + "/pathep-[eth" + interface_id + "]]","tDn":"topology/pod-1/paths-" + node_id + "/pathep-[eth" + interface_id + "]"}}}]}}
    response = session.post(url, json=json, verify=False)
    
    if response.status_code == 200:
        print ('Interface eth' + interface_id + " do node " + node_id + " configurada com sucesso!")
    else:
        print ('Deu ruim')

session = login()

with open('description.csv') as arquivo:
    csv_reader = csv.reader(arquivo)
    next(csv_reader)
    for row in csv_reader:
        set_interface_description(session, apic=apic, node_id=row[0], interface_id=row[1], interface_description=row[2])

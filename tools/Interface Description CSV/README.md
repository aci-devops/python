# Interface Description usando CSV



## Sobre o script

Este script em python adiciona uma descrição na interface dos nodes (leaf/spine) do ACI, utilizando uma planilha csv.


## Quando utilizar?

Para executar esse script, basta editar a planilha description.csv conforme a sua necessidade.

## Como utilizar?

```
python3 interface-description-csv.py
```

## Autor
Freitas®

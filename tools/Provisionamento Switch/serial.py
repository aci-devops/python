import getpass
import serial
import time

gateway = "10.10.20.14"
subnet = "/24"
str_password = "C1sco12345"


ser = serial.Serial()
ser.port = "/dev/tty.usbserial"
ser.baudrate = 9600
ser.bytesize = serial.EIGHTBITS
ser.parity = serial.PARITY_NONE
ser.stopbits = serial.STOPBITS_ONE
ser.timeout = 1           
ser.xonxoff = False     
ser.rtscts = False     
ser.dsrdtr = False       
ser.writeTimeout = 2 

with open("oob_addresses.txt") as f1:
    oob_addresses = f1.read().splitlines()

ser.open()

# Disabilitando o POAP
ser.write(b'\n')
time.sleep(1)
ser.write(b'yes\n')
time.sleep(30)

# Enforce Secure Password
ser.write(b'yes\n')
time.sleep(1)

# Setando o admin password
byte_password = bytes(str_password + '\n', 'utf8')
ser.write(byte_password)
time.sleep(1)
ser.write(byte_password)
time.sleep(1)

# Desabiliando o dialogo inicial
ser.write(b'no\n')
time.sleep(7)


ser.write(b'\n')
time.sleep(1)
ser.write(b'\n')
time.sleep(1)

# Log in
ser.write(b'admin\n')
time.sleep(1)
ser.write(byte_password)
time.sleep(1)

# conf t
ser.write(b'configure terminal\n')
time.sleep(1)

# int mgmt 
ser.write(b'interface mgmt 0\n')
time.sleep(1)
ser.write(b'no shutdown\n')
time.sleep(1)
mgmt_ip = bytes('ip address ' + oob_addresses[0] + subnet + '\n', 'utf-8')
mgmt_ip = bytes('ip address ' + oob_addresses[0] + subnet + '\n', 'utf-8')
ser.write(mgmt_ip)
time.sleep(1)
ser.write(b'exit\n')
time.sleep(1)

# Adicionando rota default
ser.write(b'vrf context management\n')
time.sleep(1)
def_route = bytes('ip route 0.0.0.0/0 ' + subnet + ' vrf management\n', 'utf-8')
def_route = bytes('ip route 0.0.0.0/0 ' + gateway + ' vrf management\n', 'utf-8')
ser.write(def_route)
time.sleep(1)

# Habilitando features
ser.write(b'feature ssh')
time.sleep(1)
ser.write(b'feature scp')
time.sleep(1)
ser.write(b'feature sftp-server')
time.sleep(1)
ser.write(b'feature nxapi')
time.sleep(1)
ser.write(b'crypto key generate rsa\n')
time.sleep(1)

# Savando as configurações
ser.write(b'copy run start\n')
time.sleep(8)

ser.close()


with open('hosts', 'a+') as f3:
    f3.writelines(oob_addresses[0] + '\n')

with open('hosts', 'r') as f:
    hosts = f.read().splitlines()

try:
    hosts_pw_index = hosts.index('password=C1sco12345')
    hosts[hosts_pw_index] = 'password=' + str_password
except:
    pass

with open('hosts', 'w') as f:
    for item in hosts:
        f.writelines("%s\n" % item)

del oob_addresses[0]

with open('oob_addresses.txt', 'w') as f1:
    for item in oob_addresses:
        f1.writelines("%s\n" % item)
        

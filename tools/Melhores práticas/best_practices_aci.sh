#!/bin/sh


##############################################################################

# Modo de Uso:
#  chmod +x bp-config.sh
#  bp-config.sh enable
#  bp-config.sh disable


##############################################################################

#USER=admin
#PASS=!v3G@!4@Y
#HOST=sandboxapicdc.cisco.com

USER=admin
PASS=C1sco12345
HOST=10.10.20.14

export MCP=YES
export REL_ESC=YES
export EP_LOOP_DETECTION=YES
export IP_AGING=YES
export ROGUE_EP_DETECTION=YES
export COOP_GROUP_POLICY=YES
export BFD_FABRIC_INT=YES
export PRESERVE_COS=YES
export PORT_TRACKING=YES


COOKIEFILE="/Users/rodrigofreitas/Desktop/cookie-file"

CURL_OPTS='-s -k -H "Content-Type: application/xml" -X POST'


##############################################################################
# Variáveis
##############################################################################

export DATE=$(date +%Y%m%d)

##############################################################################
# Funções
##############################################################################

auth() {
	curl $CURL_OPTS https://$HOST/api/mo/aaaLogin.xml -d "<aaaUser name=$USER pwd=$PASS/>" -c $COOKIEFILE > /dev/null
}


skip() {
	echo "$OPTION não está configurado no script. Para habilitar essa ação, mude  $OPTION para \"YES\"". 
	#echo "Skipping..."
	return 1
}

 
##############################################################################
### Mis-cabling Protocol
##############################################################################

# Fabric > Access Policies > Global Policies > MCP Instance Policy Default

enable_mcp() {
	if [ $MCP = "YES" ]; then 
		echo "Ativando MisCabling Protocol"
		curl $CURL_OPTS https://$HOST/api/node/mo/uni/infra/mcpInstP-default.xml -d '<mcpInstPol adminSt="enabled" key="C1sc0123" annotation="" ctrl="pdu-per-vlan" descr="" dn="uni/infra/mcpInstP-default" initDelayTime="180" loopDetectMult="2" loopProtectAct="port-disable" name="default" nameAlias="" ownerKey="" ownerTag="" txFreq="2" txFreqMsec="0"/>' -b $COOKIEFILE  > /dev/null
	else	
		OPTION="MCP" 
		skip 
	fi
}


disable_mcp() {
	if [ $MCP = "YES" ]; then 
		echo "Desativando MisCabling Protocol"
		curl $CURL_OPTS https://$HOST/api/node/mo/uni/infra/mcpInstP-default.xml -d '<mcpInstPol adminSt="disabled"/>' -b $COOKIEFILE > /dev/null
	else	
		OPTION="MCP"
		skip 
	fi
}


##############################################################################
### Disable Remote Endpoint Learning 
### Enforce Subnet Check
##############################################################################

# System > System Settings > Fabric Wide Setting

enable_rel_esc() {
	if [ $REL_ESC = "YES" ]; then
		echo "Ativando \"Disable Remote Endpoint Learning\" e \"Enforce Subnet Check\""
		curl $CURL_OPTS  https://$HOST/api/node/mo/uni/infra/settings.xml -d '<infraSetPol enforceSubnetCheck="yes" unicastXrEpLearnDisable="yes"/>' -b $COOKIEFILE > /dev/null
	else
		OPTION="REL_ESC"
		skip
	fi
}


disable_rel_esc() {
	if [ $REL_ESC = "YES" ]; then
		echo "Desativando o \"Disable Remote Endpoint Learning\" e \"Enforce Subnet Check\""
		curl $CURL_OPTS https://$HOST/api/node/mo/uni/infra/settings.xml -d '<infraSetPol enforceSubnetCheck="no" unicastXrEpLearnDisable="no"/>' -b $COOKIEFILE > /dev/null
	else   
		OPTION="REL_ESC"
		skip
	fi
}



##############################################################################
### Endpoint Loop Detection 
##############################################################################

# System > System Settings > Endpoint Controls > EP Loop Detection

enable_eploop() {
	if [[ $EP_LOOP_DETECTION = "YES" ]]; then
		echo "Ativando Endpoint Loop Protection"
		curl $CURL_OPTS https://$HOST/api/node/mo/uni/infra/epLoopProtectP-default.xml -d '<epLoopProtectP action="" adminSt="enabled" annotation="" loopDetectIntvl="60" loopDetectMult="4" />' -b $COOKIEFILE > /dev/null
	else
		OPTION="EP_LOOP_PROTECTION"
		skip
	fi
}

disable_eploop() {
	if [[ $EP_LOOP_DETECTION = "YES" ]]; then
		echo "Desativando Endpoint Loop Protection."
		curl $CURL_OPTS -X POST https://$HOST/api/node/mo/uni/infra/epLoopProtectP-default.xml -d '<epLoopProtectP action="" adminSt="disabled" annotation="" loopDetectIntvl="60" loopDetectMult="4" />' -b $COOKIEFILE > /dev/null
	else
		OPTION="EP_LOOP_PROTECTION"
		skip
	fi
}

##############################################################################
### Enable IP Aging
##############################################################################

# System > System Settings > Endpoint Control 

enable_ipaging() {
	if [[ $IP_AGING = "YES" ]]; then
		echo "Ativando IP Aging"
		curl $CURL_OPTS https://$HOST/api/node/mo/uni/infra/ipAgingP-default.xml -d '<epIpAgingP adminSt="enabled"/>' -b $COOKIEFILE > /dev/null
	else
		OPTION="IP_AGING"
		skip
	fi
}

disable_ipaging() {
	if [[ $IP_AGING = "YES" ]]; then
		echo "Desativando IP Aging" 
		curl $CURL_OPTS https://$HOST/api/node/mo/uni/infra/ipAgingP-default.xml -d '<epIpAgingP adminSt="disabled"/>' -b $COOKIEFILE > /dev/null
	else
		OPTION="IP_AGING"
		skip
	fi
}

##############################################################################
### Rogue Endpoint Detection
##############################################################################

# System > System Settings > Endpoint Controls > Rogue EP Control

enable_red() {
        if [[ $ROGUE_EP_DETECTION = "YES" ]]; then
                echo "Ativando Rogue Endpoint Detection"
		curl $CURL_OPTS https://$HOST/api/node/mo/uni/infra/epCtrlP-default.xml -d '<epControlP adminSt="enabled" rogueEpDetectIntvl="30" rogueEpDetectMult="6"/>' -b $COOKIEFILE > /dev/null
        else
                OPTION="ROGUE_EP_DETECTION"
                skip
        fi
}

disable_red() {
        if [[ $ROGUE_EP_DETECTION = "YES" ]]; then
                echo "Desativando Rogue Endpoint Detection"
		curl $CURL_OPTS https://$HOST/api/node/mo/uni/infra/epCtrlP-default.xml -d '<epControlP adminSt="disabled"/>' -b $COOKIEFILE > /dev/null
        else
                OPTION="ROGUE_EP_DETECTION"
                skip
        fi
}

##############################################################################
### Strict COOP Strict
##############################################################################

# System > System Settings > COOP Group

enable_coop() {
        if [[ $COOP_GROUP_POLICY = "YES" ]]; then
                echo "Setando o COOP para Strict Mode"
		curl $CURL_OPTS  https://$HOST/api/node/mo/uni/fabric/pol-default.xml -d '<coopPol type="strict"/>' -b $COOKIEFILE > /dev/null
        else
                OPTION="COOP_GROUP_POLICY"
                skip
        fi
}

disable_coop() {
        if [[ $COOP_GROUP_POLICY = "YES" ]]; then
                echo "Desativando COOP Strict"
		curl $CURL_OPTS https://$HOST/api/node/mo/uni/fabric/pol-default.xml -d '<coopPol type="compatible"/>' -b $COOKIEFILE > /dev/null
        else
                OPTION="COOP_GROUP_POLICY"
                skip
        fi
}

##############################################################################
### BFD para Interfaces do Fabric
##############################################################################

# Fabric > Fabric Policies > Policies > L3 Interface > default > BFD ISIS Policy Configuration

enable_bfd() {
        if [[ $BFD_FABRIC_INT = "YES" ]]; then
                echo "Ativando BFD para Interfaces do Fabric"
		curl $CURL_OPTS https://$HOST/api/node/mo/uni/fabric/l3IfP-default.xml -d '<l3IfPol bfdIsis="enabled"/>' -b $COOKIEFILE > /dev/null
        else
                OPTION="BFD_FABRIC_INT"
                skip
        fi
}

disable_bfd() {
        if [[ $BFD_FABRIC_INT = "YES" ]]; then
                echo "Desativando BFD para Interfaces do Fabric"
		curl $CURL_OPTS https://$HOST/api/node/mo/uni/fabric/l3IfP-default.xml -d '<l3IfPol bfdIsis="disabled"/>' -b $COOKIEFILE > /dev/null
        else
                OPTION="BFD_FABRIC_INT"
                skip
        fi
}

  
##############################################################################
### Preserve COS 
##############################################################################

# Fabric > Access Policies > Policies > Global > QOS Class > Preserve COS

enable_cos() {
        if [[ $PRESERVE_COS == "YES" ]]; then
                echo "Setando Preserve COS"
		curl $CURL_OPTS  https://$HOST/api/node/mo/uni/infra/qosinst-default.xml -d '<qosInstPol name="default" ctrl="dot1p-preserve"></qosInstPol>' -b $COOKIEFILE > /dev/null
        else
                OPTION="PRESERVE_COS"
                skip
        fi
}

disable_cos() {
        if [[ $PRESERVE_COS == "YES" ]]; then
                echo "Desativando Preserve COS"
		curl $CURL_OPTS  https://$HOST/api/node/mo/uni/infra/qosinst-default.xml -d '<qosInstPol name="default" ctrl=""></qosInstPol>' -b $COOKIEFILE > /dev/null
        else
                OPTION="PRESERVE_COS"
                skip
        fi
}

##############################################################################
### Enable Port Tracking
##############################################################################

enable_porttrack() {
        if [[ $PORT_TRACKING == "YES" ]]; then 
                echo "Ativando Port Tracking"
		curl $CURL_OPTS https://$HOST/api/node/mo/uni/infra/trackEqptFabP-default.xml -d '<infraPortTrackPol adminSt="on"/>' -b $COOKIEFILE > /dev/null
        else    
                OPTION="PORT_TRACKING"
                skip
        fi
}

disable_porttrack() {
        if [[ $PORT_TRACKING == "YES" ]]; then
                echo "Desativando Port Tracking"
		curl $CURL_OPTS https://$HOST/api/node/mo/uni/infra/trackEqptFabP-default.xml -d '<infraPortTrackPol adminSt="off"/>' -b $COOKIEFILE > /dev/null
        else
                OPTION="PORT_TRACKING"
                skip
        fi
}

enable_all() {
	enable_mcp 
	enable_rel_esc
	enable_eploop
	enable_ipaging
	enable_red
	enable_coop
	enable_bfd
	enable_cos
	enable_porttrack
}


disable_all() {
	disable_mcp
	disable_rel_esc
	disable_eploop
	disable_ipaging
	disable_red
	disable_coop
	disable_bfd
	disable_cos
	disable_porttrack
}

##############################################################################
# Main
##############################################################################

case $1 in 
	enable ) 	auth
			enable_all
			;;
	disable	)	auth	
			disable_all
			;;
	* )		usage
			exit 1
			;;
esac

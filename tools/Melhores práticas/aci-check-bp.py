## aci-check-bp
#!/usr/bin/env python3
# coding: utf-8

# Esse script foi criado para validar as melhores práticas fornecida pela Cisco.

import requests
import json
import os

# Limpa tela
os.system('cls' if os.name == 'nt' else 'clear')
 
# Puxando o arquivo com as credenciais
from config import controller, username, password
base_url = "https://" + str(controller) + "/api/"
auth_bit = "aaaLogin.json"
auth_url = base_url + auth_bit
auth_data = {
  "aaaUser":{
    "attributes":{
      "name":username,
      "pwd":password
    }
  }
}

# ## Criando a requisição
requests.packages.urllib3.disable_warnings() 
s = requests.session()
s.post(auth_url, json=auth_data, verify=False)

## Verificando se as melhores práticas estão habilitadas 

### Enforce Subnet Check deve estar setado como enforceSubnetCheck:yes
### Disable Remote EP Learn deve estar setado como unicastXrEpLearnDisable:yes
### Domain Validation deve estar setado como domainValidation:yes
### System > System Settings > Fabric Wide Setting
FabWide_class="node/class/infraSetPol.json"
FabWide_url = base_url + FabWide_class

FabWide = s.get(FabWide_url, verify=False)
FabWide_out = FabWide.json()

NoBP_count = 0

### Fabric Wide 
FabWideList = FabWide_out['imdata']
IsEnforceActivated = FabWideList[0]['infraSetPol']['attributes']['enforceSubnetCheck']
IsDisXRLearnActivated = FabWideList[0]['infraSetPol']['attributes']['unicastXrEpLearnDisable']
IsDomValActivated = FabWideList[0]['infraSetPol']['attributes']['domainValidation']

### Port Tracking
### Port Tracking deve estar setado como  adminSt:on
### System > System Settings > Port Tracking
PortTrack_class="node/class/infraPortTrackPol.json"
PortTrack_url = base_url + PortTrack_class

PortTrack = s.get(PortTrack_url, verify=False)
PortTrack_out = PortTrack.json()

### Port Tracking
PortTrackList = PortTrack_out['imdata']
PortTrackAdminState = PortTrackList[0]['infraPortTrackPol']['attributes']['adminSt']

### EP Loop Protection 
### EP Loop Detection deve estar setado como adminSt:enabled
### EP Loop Detection Action settings deve estar setado como action:"" << vazio
### System > System Settings > Endpoint Controls > EP Loop Detection
EPLoop_class="node/class/epLoopProtectP.json"
EPLoop_url = base_url + EPLoop_class

EPLoop = s.get(EPLoop_url, verify=False)
EPLoop_out = EPLoop.json()

### EP Loop Detection
EPLoopList = EPLoop_out['imdata']
EPLoopAdminState = EPLoopList[0]['epLoopProtectP']['attributes']['adminSt']
EPLoopAction = EPLoopList[0]['epLoopProtectP']['attributes']['action']

### Rogue EP Protection 
### Rogue EP Status deve estar setado como adminSt:enabled
### System > System Settings > Endpoint Controls > Rogue EP Control
Rogue_class="node/class/epControlP.json"
Rogue_url = base_url + Rogue_class

Rogue = s.get(Rogue_url, verify=False)
Rogue_out = Rogue.json()

### Rogue EP 
RogueList = Rogue_out['imdata']
RogueAdminState = RogueList[0]['epControlP']['attributes']['adminSt']

### IP Aging 
### IP Aging deve estar setado como adminSt:enabled
### System > System Settings > Endpoint Controls > IP Aging
IPAging_class="node/class/epIpAgingP.json"
IPAging_url = base_url + IPAging_class

IPAging = s.get(IPAging_url, verify=False)
IPAging_out = IPAging.json()

### IP Aging 
IPAgingList = IPAging_out['imdata']
IPAgingAdminState = IPAgingList[0]['epIpAgingP']['attributes']['adminSt']

### MCP 
### MCP deve estar setado como adminSt:enabled and ctrl:pdu-per-vlan
### Fabric > Access Policies > Global Policies > MCP Instance Policy default
MCP_class="node/class/mcpInstPol.json"
MCP_url = base_url + MCP_class

MCP = s.get(MCP_url, verify=False)
MCP_out = MCP.json()

### MCP 
MCPList = MCP_out['imdata']
MCPAdminState = MCPList[0]['mcpInstPol']['attributes']['adminSt']
MCP_PerEPG = MCPList[0]['mcpInstPol']['attributes']['ctrl']

### BFD Internal fabric 
### BFD deve estar setado como bfdIsis:enabled
### Fabric > Fabric Policies > Policies > Interface > L3 Interface > default > BFD ISIS Policy Configuration
BFD_class="node/class/l3IfPol.json"
BFD_url = base_url + BFD_class

BFD = s.get(BFD_url, verify=False)
BFD_out = BFD.json()

### BFD
BFDList = BFD_out['imdata']
BFDAdminState = BFDList[0]['l3IfPol']['attributes']['bfdIsis']


### Coop Strict  
### Coop deve estar setado como type:strict
### System > System Settings > COOP Group
CoopPol_class="node/class/coopPol.json"
CoopPol_url = base_url + CoopPol_class

CoopPol = s.get(CoopPol_url, verify=False)
CoopPol_out = CoopPol.json()

### Coop 
CoopPolList = CoopPol_out['imdata']
CoopPolAdminState = CoopPolList[0]['coopPol']['attributes']['type']


### Preserve COS  
### Preserve COS deve estar setado como ctrl:dot1p-preserve
### Fabric > Access Policies > Policies > Global > QOS Class > Preserve COS
PCOS_class="node/class/qosInstPol.json"
PCOS_url = base_url + PCOS_class

PCOS = s.get(PCOS_url, verify=False)
PCOS_out = PCOS.json()

### Preserve COS Variables
PCOSList = PCOS_out['imdata']
PCOSAdminState = PCOSList[0]['qosInstPol']['attributes']['ctrl']

 

print('========================================================================')
print("\n+*+*+ Checando as configurações de Melhores Práticas. +*+*+\n")
 
#cores
vermelho = '\033[1;31m'
verde = '\033[1;32m'
amarelo = '\033[1;33m'

if MCPAdminState == "disabled" or MCP_PerEPG == "":
  print(f'{vermelho}MisCabling Protocol (MCP):')
  print("**** Warning: MCP está desabilitado globalmente ou a opção MCP Per Vlan não está habilitada. ****")
  print("Na aba Fabric > Access Policies > Global Policies > MCP Instance Policy default para habilitar.\n")
else:
  print(f'{verde}MisCabling Protocol (MCP):')
  print("MisCabling Protocol (MCP) está seguindo as Melhores práticas.")
  print("MCP state é", MCPAdminState, "e o Per-EPG-Config é", MCP_PerEPG, "\n")  

if IsEnforceActivated != "yes":
  print(f'{vermelho}Enforced Subnet Check:')
  print("**** Warning: EnforceSubnetCheck não está habilitado. ****")
  print("Em  System > System Settings > Fabric Wide Setting para habilitar.\n")
else:
  print(f'{verde}Enforced Subnet Check:')  
  print("Enforced Subnet Check está seguindo as Melhores práticas.")
  print("Enforce Subnet Check está habilitado.\n") 

if IsDisXRLearnActivated != "yes":
  print(f'{vermelho}Disable Remote EP Learn:')
  print("**** Warning: Disable Remote EP Learn não está habilitado. ****")
  print("Em System > System Settings > Fabric Wide Setting para habilitar.\n")
else:
  print(f'{verde}Disable Remote EP Learn:')  
  print("Disable Remote EP Learn está seguindo as Melhores práticas.")
  print("Disable Remote EP Learn está habilitado.\n")

if IsDomValActivated != "yes":
  print(f'{vermelho}Domain Validation:')
  print("**** Warning: Domain Validation não está habilitado.")
  print("Em System > System Settings > Fabric Wide Setting para habilitar.\n")
else:
  print(f'{verde}Domain Validation:')  
  print("Domain Validation está seguindo as Melhores práticas.")
  print("Domain Validation está habilitado.\n")

if PortTrackAdminState !="on":
  print(f'{vermelho}Uplink Port Tracking:')
  print("**** Warning: Uplink Port Tracking não está habilitado.")
  print("Em System > System Settings > Port Tracking para habilitar.\n")
else:
  print(f'{verde}Uplink Port Tracking:') 
  print("Uplink Port Tracking está seguindo as Melhores práticas.")
  print("Uplink Port Tracking está como", PortTrackAdminState, "\n")

if EPLoopAdminState !="enabled":
  print(f'{vermelho}Endpoint Loop Detection:')
  print("**** Warning: EP Loop Detection não está habilitado.")
  print("Em System > System Settings > Endpoint Controls para habilitar.\n")
elif EPLoopAdminState == "enabled" and EPLoopAction != "":
  print(f'{amarelo}Endpoint Loop Detection:')
  print("**** Warning: EP Loop Detection está habilitado, porém o recomendado é desabilitar as ações que estão atualmente definidas para", EPLoopAction)
  print("Em System > System Settings > Endpoint Controls para desabilitar essas ações.\n")
else:
  print(f'{verde}Endpoint Loop Detection:') 
  print("Endpoint Loop Detection está seguindo as Melhores práticas.")
  print("Endpoint Loop Detection está como", PortTrackAdminState, "e Endpoint Actions não estão configurados.\n")

if RogueAdminState != "enabled":
  print(f'{vermelho}Rogue Endpoint Control:')
  print("**** Warning: Rogue EP Control não está habilitado.")
  print("Em System > System Settings > Endpoint Controls > Rogue EP Control para habilitar.\n")
else:
  print(f'{verde}Rogue Endpoint Control:')
  print("Rogue Endpoint Control está seguindo as Melhores práticas.")
  print("Rogue EP Control está como", RogueAdminState, "\n")

if IPAgingAdminState != "enabled":
  print(f'{vermelho}IP Aging:')
  print("**** Warning: IP Aging não está habilitado.")
  print("Em System > System Settings > Endpoint Controls > IP Aging para habilitar.\n")
else:
  print(f'{verde}IP Aging:')
  print("IP Aging está seguindo as Melhores práticas.")
  print("IP Aging está como", IPAgingAdminState, "\n")

if BFDAdminState !="enabled":
  print(f'{vermelho}Internal Fabric BFD:')
  print("**** Warning: Internal Fabric BFD não está habilitado.")
  print("Em System > Fabric Policies > Policies > Interface > L3 Interface > default > BFD ISIS Policy Configuration para habilitar.\n")
else:
  print(f'{verde}Internal Fabric BFD:')  
  print("Internal Fabric BFD está seguindo as Melhores Práticas.")
  print("Internal Fabric BFD está como", BFDAdminState, "\n")

if CoopPolAdminState != "strict":
  print(f'{vermelho}Coop Policy mode:')
  print("**** Warning: Sua política de COOP não está setada como strict.")
  print("Em System > System Settings > COOP Group para configurar.\n")
else:
  print(f'{verde}Coop Policy mode:')  
  print("Coop Policy mode está seguindo as Melhores práticas.")
  print("Coop Policy está como", CoopPolAdminState, "\n")

if PCOSAdminState != "dot1p-preserve":
  print(f'{vermelho}Preserve COS:')
  print("**** Warning: Seu Fabric não está configurado com os valores de preserve COS.")
  print("Na aba Fabric > Access Policies > Policies > Global > QOS Class > Preserve COS para configurar.\n")
else:
  print(f'{verde}Preserve COS:')  
  print("Preserve COS setting está seguindo as Melhores práticas.")
  print("Preserve COS policy está como", PCOSAdminState, "\n")

print('\n========================================================================')

#file: /tmp/apic-collect-v5.sh
#Copyright (c) by Freitas
#

DATA=`date +"%Y-%m-%d"`

echo "Get Endpoints (fvCEp) [01/44]..."
moquery -c fvCEp                                  > /tmp/collect-apic-$HOSTNAME-p01-fvCEp.txt
moquery -c fvCEp -x 'rsp-subtree=children'        > /tmp/collect-apic-$HOSTNAME-p01-fvCEp-children.txt
moquery -c fvCEp -o xml                           > /tmp/collect-apic-$HOSTNAME-p01-fvCEp.xml
moquery -c fvCEp -x 'rsp-subtree=children' -o xml > /tmp/collect-apic-$HOSTNAME-p01-fvCEp-children.xml

echo "Endpoints - path [02/44]..."
moquery -c fvRsCEpToPathEp                                  > /tmp/collect-apic-$HOSTNAME-p02-fvRsCEpToPathEp.txt
moquery -c fvRsCEpToPathEp -x 'rsp-subtree=children'        > /tmp/collect-apic-$HOSTNAME-p02-fvRsCEpToPathEp-children.txt
moquery -c fvRsCEpToPathEp -o xml                           > /tmp/collect-apic-$HOSTNAME-p02-fvRsCEpToPathEp.xml
moquery -c fvRsCEpToPathEp -x 'rsp-subtree=children' -o xml > /tmp/collect-apic-$HOSTNAME-p02-fvRsCEpToPathEp-children.xml

echo "some Objects from APIC [03/44]..."
moquery -c fvTenant -o table                     >  /tmp/collect-apic-$HOSTNAME-p03-Objects-table.txt
moquery -c fvCtx -o table                        >> /tmp/collect-apic-$HOSTNAME-p03-Objects-table.txt
moquery -c fvBD -o table                         >> /tmp/collect-apic-$HOSTNAME-p03-Objects-table.txt
moquery -c fvSubnet -o table                     >> /tmp/collect-apic-$HOSTNAME-p03-Objects-table.txt
moquery -c fvAp -o table                         >> /tmp/collect-apic-$HOSTNAME-p03-Objects-table.txt
moquery -c fvAEPg -o table                       >> /tmp/collect-apic-$HOSTNAME-p03-Objects-table.txt
moquery -c fvRsDomAtt -o table                   >> /tmp/collect-apic-$HOSTNAME-p03-Objects-table.txt
moquery -c fvRsDomAtt -o table | grep VMware     >> /tmp/collect-apic-$HOSTNAME-p03-Objects-table.txt
moquery -c l3extRsNodeL3OutAtt -o table          >> /tmp/collect-apic-$HOSTNAME-p03-Objects-table.txt
moquery -c dhcpRelayP -o table                   >> /tmp/collect-apic-$HOSTNAME-p03-Objects-table.txt
moquery -c vzBrCP -o table                       >> /tmp/collect-apic-$HOSTNAME-p03-Objects-table.txt
moquery -c vzFilter -o table                     >> /tmp/collect-apic-$HOSTNAME-p03-Objects-table.txt

echo "Get all Tenants [04/44]..."
moquery -c fvTenant                                   >  /tmp/collect-apic-$HOSTNAME-p04-fvTenant.txt
moquery -c fvTenant -o table                          >  /tmp/collect-apic-$HOSTNAME-p04-fvTenant-table.xml
moquery -c fvTenant -o xml                            >  /tmp/collect-apic-$HOSTNAME-p04-fvTenant.xml
moquery -c fvTenant  -x 'rsp-subtree=children'        >  /tmp/collect-apic-$HOSTNAME-p04-fvTenant-children.txt
moquery -c fvTenant  -x 'rsp-subtree=children' -o xml >  /tmp/collect-apic-$HOSTNAME-p04-fvTenant-children.xml

echo "Get all VRFS [05/44]..."
moquery -c fvCtx                                  >  /tmp/collect-apic-$HOSTNAME-p05-fvCtx.txt
moquery -c fvCtx -o table                         >  /tmp/collect-apic-$HOSTNAME-p05-fvCtx-table.txt
moquery -c fvCtx -o xml                           >  /tmp/collect-apic-$HOSTNAME-p05-fvCtx.xml
moquery -c fvCtx -x 'rsp-subtree=children'        >  /tmp/collect-apic-$HOSTNAME-p05-fvCtx-children.txt
moquery -c fvCtx -x 'rsp-subtree=children' -o xml >  /tmp/collect-apic-$HOSTNAME-p05-fvCtx-children.xml
moquery -c fvCtx -o table | grep unenforced       >  /tmp/collect-apic-$HOSTNAME-p05-fvCtx-unenforced.txt
moquery -c fvCtx -o table | grep enforced         >  /tmp/collect-apic-$HOSTNAME-p05-fvCtx-enforced.txt
#Get BDs associate to VRFs
moquery -c fv.RtCtx  -o xml                       >  /tmp/collect-apic-$HOSTNAME-p05-RtCtx.xml
moquery -c fv.RtCtx | grep dn                     >  /tmp/collect-apic-$HOSTNAME-p05-RtCtx.txt

echo "Get all BDs [06/44]..."
moquery -c fvBD                                      >  /tmp/collect-apic-$HOSTNAME-p06-fvBD.txt
moquery -c fvBD  -o table                            >  /tmp/collect-apic-$HOSTNAME-p06-fvBD-table.txt
moquery -c fvBD  -o xml                              >  /tmp/collect-apic-$HOSTNAME-p06-fvBD.xml
moquery -c fvBD  -x 'rsp-subtree=children'           >  /tmp/collect-apic-$HOSTNAME-p06-fvBD-children.txt
moquery -c fvBD  -x 'rsp-subtree=children' -o xml    >  /tmp/collect-apic-$HOSTNAME-p06-fvBD-children.xml
#Get Resolution Bride Domain
moquery -c fv.RsBd  -o xml                           >  /tmp/collect-apic-$HOSTNAME-p06-RsBd.xml

echo "Get all Subnet [07/44]..."
moquery -c fvSubnet                                     > /tmp/collect-apic-$HOSTNAME-p07-fvSubnet.txt
moquery -c fvSubnet -o table                            > /tmp/collect-apic-$HOSTNAME-p07-fvSubnet-table.txt
moquery -c fvSubnet -o xml                              > /tmp/collect-apic-$HOSTNAME-p07-fvSubnet.xml
moquery -c fvSubnet  -x 'rsp-subtree=children'          > /tmp/collect-apic-$HOSTNAME-p07-fvSubnet-children.txt
moquery -c fvSubnet  -x 'rsp-subtree=children' -o xml   > /tmp/collect-apic-$HOSTNAME-p07-fvSubnet-children.xml

echo "Get all Application Profile [08/44]..."
moquery -c fvAp                                         > /tmp/collect-apic-$HOSTNAME-p08-fvAp.txt
moquery -c fvAp -o table                                > /tmp/collect-apic-$HOSTNAME-p08-fvAp-table.txt
moquery -c fvAp -o xml                                  > /tmp/collect-apic-$HOSTNAME-p08-fvAp.xml
moquery -c fvAp -x 'rsp-subtree=children'               > /tmp/collect-apic-$HOSTNAME-p08-fvAp-children.txt
moquery -c fvAp -x 'rsp-subtree=children' -o xml        > /tmp/collect-apic-$HOSTNAME-p08-fvAp-children.xml

echo "Get all EPGs [09/44]..."
moquery -c fvAEPg                                       > /tmp/collect-apic-$HOSTNAME-p09-fvAEPg.txt
moquery -c fvAEPg -o table                              > /tmp/collect-apic-$HOSTNAME-p09-fvAEPg-table.txt
moquery -c fvAEPg -o xml                                > /tmp/collect-apic-$HOSTNAME-p09-fvAEPg.xml
moquery -c fvAEPg -x 'rsp-subtree=children'             > /tmp/collect-apic-$HOSTNAME-p09-fvAEPg-children.txt
moquery -c fvAEPg -x 'rsp-subtree=children' -o xml      > /tmp/collect-apic-$HOSTNAME-p09-fvAEPg-children.xml

echo "Get Domains [10/44]..."
moquery -c fvRsDomAtt                                   > /tmp/collect-apic-$HOSTNAME-p10-fvRsDomAtt.txt
moquery -c fvRsDomAtt -o table                          > /tmp/collect-apic-$HOSTNAME-p10-fvRsDomAtt-table.txt
moquery -c fvRsDomAtt -o xml                            > /tmp/collect-apic-$HOSTNAME-p10-fvRsDomAtt.xml
moquery -c fvRsDomAtt -x 'rsp-subtree=children'         > /tmp/collect-apic-$HOSTNAME-p10-fvRsDomAtt-children.txt
moquery -c fvRsDomAtt -x 'rsp-subtree=children' -o xml  > /tmp/collect-apic-$HOSTNAME-p10-fvRsDomAtt-children.xml
moquery -c fvRsDomAtt   | grep VMware                   > /tmp/collect-apic-$HOSTNAME-p10-fvRsDomAtt-VMware.txt

echo "Get L3OUT [11/44]..."
moquery -c l3extOut                                     > /tmp/collect-apic-$HOSTNAME-p11-l3extOut.txt
moquery -c l3extOut -o xml                              > /tmp/collect-apic-$HOSTNAME-p11-l3extOut.xml
moquery -c l3extOut -x 'rsp-subtree=children'           > /tmp/collect-apic-$HOSTNAME-p11-l3extOut-children.txt
moquery -c l3extOut -x 'rsp-subtree=children' -o xml    > /tmp/collect-apic-$HOSTNAME-p11-l3extOut-children.xml
#
moquery -c l3extRsNodeL3OutAtt                                    > /tmp/collect-apic-$HOSTNAME-p11-l3extRsNodeL3OutAtt.txt
moquery -c l3extRsNodeL3OutAtt -o xml                             > /tmp/collect-apic-$HOSTNAME-p11-l3extRsNodeL3OutAtt.xml
moquery -c l3extRsNodeL3OutAtt -x 'rsp-subtree=children'          > /tmp/collect-apic-$HOSTNAME-p11-l3extRsNodeL3OutAtt-children.txt
moquery -c l3extRsNodeL3OutAtt -x 'rsp-subtree=children' -o xml   > /tmp/collect-apic-$HOSTNAME-p11-l3extRsNodeL3OutAtt-children.xml

echo "Get dhcpRelayP [12/44]..."
moquery -c dhcpRelayP                                    > /tmp/collect-apic-$HOSTNAME-p12-dhcpRelayP.txt
moquery -c dhcpRelayP -o xml                             > /tmp/collect-apic-$HOSTNAME-p12-dhcpRelayP.xml
moquery -c dhcpRelayP -x 'rsp-subtree=children'          > /tmp/collect-apic-$HOSTNAME-p12-dhcpRelayP-children.xml
moquery -c dhcpRelayP -x 'rsp-subtree=children' -o xml   > /tmp/collect-apic-$HOSTNAME-p12-dhcpRelayP-children.txt

echo "Get vzBrCP [13/44]..."
moquery -c vzBrCP                                     > /tmp/collect-apic-$HOSTNAME-p13-vzBrCP.txt
moquery -c vzBrCP -o xml                              > /tmp/collect-apic-$HOSTNAME-p13-vzBrCP.xml
moquery -c vzBrCP -x 'rsp-subtree=children'           > /tmp/collect-apic-$HOSTNAME-p13-vzBrCP-children.txt
moquery -c vzBrCP -x 'rsp-subtree=children' -o xml    > /tmp/collect-apic-$HOSTNAME-p13-vzBrCP-children.xml

echo "Get Filter [14/44]..."
moquery -c vzFilter                                   > /tmp/collect-apic-$HOSTNAME-p14-vzFilter.txt
moquery -c vzFilter -o xml                            > /tmp/collect-apic-$HOSTNAME-p14-vzFilter.xml
moquery -c vzFilter -x 'rsp-subtree=children'         > /tmp/collect-apic-$HOSTNAME-p14-vzFilter-children.txt
moquery -c vzFilter -x 'rsp-subtree=children' -o xml  > /tmp/collect-apic-$HOSTNAME-p14-vzFilter-children.xml

echo "Get Spine Profile [15/44]..."
moquery -c fabricSpPortP                                   > /tmp/collect-apic-$HOSTNAME-p15-fabricSpPortP.txt
moquery -c fabricSpPortP -o table                          > /tmp/collect-apic-$HOSTNAME-p15-fabricSpPortP-table.txt
moquery -c fabricSpPortP -o xml                            > /tmp/collect-apic-$HOSTNAME-p15-fabricSpPortP.xml
moquery -c fabricSpPortP -x 'rsp-subtree=children'         > /tmp/collect-apic-$HOSTNAME-p15-fabricSpPortP-children.txt
moquery -c fabricSpPortP -x 'rsp-subtree=children' -o xml  > /tmp/collect-apic-$HOSTNAME-p15-fabricSpPortP-children.xml

echo "Get Spine Interface Profile [16/44]..."
moquery -c ffabricSpPortP                                     > /tmp/collect-apic-$HOSTNAME-p16-ffabricSpPortP.txt
moquery -c ffabricSpPortP -o table                            > /tmp/collect-apic-$HOSTNAME-p16-ffabricSpPortP-table.txt
moquery -c ffabricSpPortP -o xml                              > /tmp/collect-apic-$HOSTNAME-p16-ffabricSpPortP.xml
moquery -c ffabricSpPortP -x 'rsp-subtree=children'           > /tmp/collect-apic-$HOSTNAME-p16-ffabricSpPortP-children.txt
moquery -c ffabricSpPortP -x 'rsp-subtree=children' -o xml    > /tmp/collect-apic-$HOSTNAME-p16-ffabricSpPortP-children.xml

echo "Get Leaf Profile [17/44]..."
moquery -c infraAccPortP                                     > /tmp/collect-apic-$HOSTNAME-p17-infraAccPortP.txt
moquery -c infraAccPortP -o table                            > /tmp/collect-apic-$HOSTNAME-p17-infraAccPortP-table.txt
moquery -c infraAccPortP -o xml                              > /tmp/collect-apic-$HOSTNAME-p17-infraAccPortP.xml
moquery -c infraAccPortP -x 'rsp-subtree=children'           > /tmp/collect-apic-$HOSTNAME-p17-infraAccPortP-children.txt
moquery -c infraAccPortP -x 'rsp-subtree=children' -o xml    > /tmp/collect-apic-$HOSTNAME-p17-infraAccPortP-children.xml

echo "Get Leaf Interface Profile [18/44]..."
moquery -c infraNodeP                                   > /tmp/collect-apic-$HOSTNAME-p18-infraNodeP.txt
moquery -c infraNodeP -o table                          > /tmp/collect-apic-$HOSTNAME-p18-infraNodeP-table.txt
moquery -c infraNodeP -o xml                            > /tmp/collect-apic-$HOSTNAME-p18-infraNodeP.xml
moquery -c infraNodeP -x 'rsp-subtree=children'         > /tmp/collect-apic-$HOSTNAME-p18-infraNodeP-children.txt
moquery -c infraNodeP -x 'rsp-subtree=children' -o xml  > /tmp/collect-apic-$HOSTNAME-p18-infraNodeP-children.xml

echo "Get Interface Policies - speed (link level) [19/44]..."
moquery -c fabricHIfPol                                   > /tmp/collect-apic-$HOSTNAME-p19-fabricHIfPol.txt
moquery -c fabricHIfPol -o table                          > /tmp/collect-apic-$HOSTNAME-p19-fabricHIfPol-table.txt
moquery -c fabricHIfPol -o xml                            > /tmp/collect-apic-$HOSTNAME-p19-fabricHIfPol.xml
moquery -c fabricHIfPol -x 'rsp-subtree=children'         > /tmp/collect-apic-$HOSTNAME-p19-fabricHIfPol-children.txt
moquery -c fabricHIfPol -x 'rsp-subtree=children' -o xml  > /tmp/collect-apic-$HOSTNAME-p19-fabricHIfPol-children.xml

echo "Get Interface Policies - CPD Policies [20/44]..."
moquery -c cdpIfPol                                     > /tmp/collect-apic-$HOSTNAME-p20-cdpIfPol.txt
moquery -c cdpIfPol -o table                            > /tmp/collect-apic-$HOSTNAME-p20-cdpIfPol-table.txt
moquery -c cdpIfPol -o xml                              > /tmp/collect-apic-$HOSTNAME-p20-cdpIfPol.xml
moquery -c cdpIfPol  -x 'rsp-subtree=children'          > /tmp/collect-apic-$HOSTNAME-p20-cdpIfPol-children.txt
moquery -c cdpIfPol  -x 'rsp-subtree=children' -o xml   > /tmp/collect-apic-$HOSTNAME-p20-cdpIfPol-children.xml

echo "Get Interface Policies -LLDP Policies [21/44]..."
moquery -c lldpIfPol                                      > /tmp/collect-apic-$HOSTNAME-p21-lldpIfPol.txt
moquery -c lldpIfPol -o table                             > /tmp/collect-apic-$HOSTNAME-p21-lldpIfPol-table.txt
moquery -c lldpIfPol -o xml                               > /tmp/collect-apic-$HOSTNAME-p21-lldpIfPol.xml
moquery -c lldpIfPol  -x 'rsp-subtree=children'           > /tmp/collect-apic-$HOSTNAME-p21-lldpIfPol-children.txt
moquery -c lldpIfPol  -x 'rsp-subtree=children' -o xml    > /tmp/collect-apic-$HOSTNAME-p21-lldpIfPol-children.xml

echo "Get bgpInstPol [22/44]..."
moquery -c bgpInstPol                                      > /tmp/collect-apic-$HOSTNAME-p22-bgpInstPol.txt
moquery -c bgpInstPol  -o table                            > /tmp/collect-apic-$HOSTNAME-p22-bgpInstPol-table.txt
moquery -c bgpInstPol  -o xml                              > /tmp/collect-apic-$HOSTNAME-p22-bgpInstPol.xml
moquery -c bgpInstPol  -x 'rsp-subtree=children'           > /tmp/collect-apic-$HOSTNAME-p22-bgpInstPol-children.txt
moquery -c bgpInstPol  -x 'rsp-subtree=children' -o xml    > /tmp/collect-apic-$HOSTNAME-p22-bgpInstPol-children.xml

echo "Get bgpRRP [23/44]..."
moquery -c bgpRRP                                    > /tmp/collect-apic-$HOSTNAME-p23-bgpRRP.txt
moquery -c bgpRRP -o table                           > /tmp/collect-apic-$HOSTNAME-p23-bgpRRP-table.txt
moquery -c bgpRRP -o xml                             > /tmp/collect-apic-$HOSTNAME-p23-bgpRRP.xml
moquery -c bgpRRP -x 'rsp-subtree=children'          > /tmp/collect-apic-$HOSTNAME-p23-bgpRRP-children.txt
moquery -c bgpRRP -x 'rsp-subtree=children' -o xml   > /tmp/collect-apic-$HOSTNAME-p23-bgpRRP-children.xml

echo "Get fabricPodPGrp [24/44]..."
moquery -c fabricPodPGrp                                    > /tmp/collect-apic-$HOSTNAME-p24-fabricPodPGrp.txt
moquery -c fabricPodPGrp -o table                           > /tmp/collect-apic-$HOSTNAME-p24-fabricPodPGrp-table.txt
moquery -c fabricPodPGrp -o xml                             > /tmp/collect-apic-$HOSTNAME-p24-fabricPodPGrp.xml
moquery -c fabricPodPGrp -x 'rsp-subtree=children'          > /tmp/collect-apic-$HOSTNAME-p24-fabricPodPGrp-children.txt
moquery -c fabricPodPGrp -x 'rsp-subtree=children' -o xml   > /tmp/collect-apic-$HOSTNAME-p24-fabricPodPGrp-children.xml

echo "Get fabricPodS [25/44]..."
moquery -c fabricPodS                                       > /tmp/collect-apic-$HOSTNAME-p25-fabricPodS.txt
moquery -c fabricPodS -o table                              > /tmp/collect-apic-$HOSTNAME-p25-fabricPodS-table.txt
moquery -c fabricPodS -o xml                                > /tmp/collect-apic-$HOSTNAME-p25-fabricPodS.xml
moquery -c fabricPodS -x 'rsp-subtree=children'             > /tmp/collect-apic-$HOSTNAME-p25-fabricPodS-children.txt
moquery -c fabricPodS -x 'rsp-subtree=children' -o xml      > /tmp/collect-apic-$HOSTNAME-p25-fabricPodS-children.xml

echo "Get OOB Management (mgmtRsOoBStNode) [26/44]..."
moquery -c mgmtRsOoBStNode                                  > /tmp/collect-apic-$HOSTNAME-p26-mgmtRsOoBStNode.txt
moquery -c mgmtRsOoBStNode -o table                         > /tmp/collect-apic-$HOSTNAME-p26-mgmtRsOoBStNode-table.txt
moquery -c mgmtRsOoBStNode -o xml                           > /tmp/collect-apic-$HOSTNAME-p26-mgmtRsOoBStNode.xml
moquery -c mgmtRsOoBStNode -x 'rsp-subtree=children'        > /tmp/collect-apic-$HOSTNAME-p26-mgmtRsOoBStNode-children.txt
moquery -c mgmtRsOoBStNode -x 'rsp-subtree=children' -o xml > /tmp/collect-apic-$HOSTNAME-p26-mgmtRsOoBStNode-children.xml

echo "Get VLAN POOL (fvnsVlanInstP) [27/44]..."
moquery -c fvnsVlanInstP                                  > /tmp/collect-apic-$HOSTNAME-p27-fvnsVlanInstP.txt
moquery -c fvnsVlanInstP -o table                         > /tmp/collect-apic-$HOSTNAME-p27-fvnsVlanInstP-table.txt
moquery -c fvnsVlanInstP -o xml                           > /tmp/collect-apic-$HOSTNAME-p27-fvnsVlanInstP.xml
moquery -c fvnsVlanInstP -x 'rsp-subtree=children'        > /tmp/collect-apic-$HOSTNAME-p27-fvnsVlanInstP-children.txt
moquery -c fvnsVlanInstP -x 'rsp-subtree=children' -o xml > /tmp/collect-apic-$HOSTNAME-p27-fvnsVlanInstP-children.xml

echo "Get AEP (infraAttEntityP) [28/44]..."
moquery -c infraAttEntityP                                  > /tmp/collect-apic-$HOSTNAME-p28-infraAttEntityP.txt
moquery -c infraAttEntityP -o table                         > /tmp/collect-apic-$HOSTNAME-p28-infraAttEntityP-table.txt
moquery -c infraAttEntityP -o xml                           > /tmp/collect-apic-$HOSTNAME-p28-infraAttEntityP.xml
moquery -c infraAttEntityP -x 'rsp-subtree=children'        > /tmp/collect-apic-$HOSTNAME-p28-infraAttEntityP-children.txt
moquery -c infraAttEntityP -x 'rsp-subtree=children' -o xml > /tmp/collect-apic-$HOSTNAME-p28-infraAttEntityP-children.xml

echo "Get Physical Domain (physDomP) [29/44]..."
moquery -c physDomP                                  > /tmp/collect-apic-$HOSTNAME-p29-physDomP.txt
moquery -c physDomP -o table                         > /tmp/collect-apic-$HOSTNAME-p29-physDomP-table.txt
moquery -c physDomP -o xml                           > /tmp/collect-apic-$HOSTNAME-p29-physDomP.xml
moquery -c physDomP -x 'rsp-subtree=children'        > /tmp/collect-apic-$HOSTNAME-p29-physDomP-children.txt
moquery -c physDomP -x 'rsp-subtree=children' -o xml > /tmp/collect-apic-$HOSTNAME-p29-physDomP-children.xml

echo "Get External Routed Domain|L3OUT (l3extDomP) [30/44]..."
moquery -c l3extDomP                                  > /tmp/collect-apic-$HOSTNAME-p30-l3extDomP.txt
moquery -c l3extDomP -o table                         > /tmp/collect-apic-$HOSTNAME-p30-l3extDomP-table.txt
moquery -c l3extDomP -o xml                           > /tmp/collect-apic-$HOSTNAME-p30-l3extDomP.xml
moquery -c l3extDomP -x 'rsp-subtree=children'        > /tmp/collect-apic-$HOSTNAME-p30-l3extDomP-children.txt
moquery -c l3extDomP -x 'rsp-subtree=children' -o xml > /tmp/collect-apic-$HOSTNAME-p30-l3extDomP-children.xml

echo "Get VMM Domain (vmmProvP) [31/44]..."
moquery -c vmmProvP                                   > /tmp/collect-apic-$HOSTNAME-p31-vmmProvP.txt
moquery -c vmmProvP -o table                          > /tmp/collect-apic-$HOSTNAME-p31-vmmProvP-table.txt
moquery -c vmmProvP -o xml                            > /tmp/collect-apic-$HOSTNAME-p31-vmmProvP.xml
moquery -c vmmProvP -x 'rsp-subtree=children'         > /tmp/collect-apic-$HOSTNAME-p31-vmmProvP-children.txt
moquery -c vmmProvP -x 'rsp-subtree=children' -o xml  > /tmp/collect-apic-$HOSTNAME-p31-vmmProvP-children.xml

echo "Get vmmDomP [32/44]..."
moquery -c vmmDomP                                    > /tmp/collect-apic-$HOSTNAME-p32-vmmDomP.txt
moquery -c vmmDomP -o table                           > /tmp/collect-apic-$HOSTNAME-p32-vmmDomP-table.txt
moquery -c vmmDomP -o xml                             > /tmp/collect-apic-$HOSTNAME-p32-vmmDomP.xml
moquery -c vmmDomP -x 'rsp-subtree=children'          > /tmp/collect-apic-$HOSTNAME-p32-vmmDomP-children.txt
moquery -c vmmDomP -x 'rsp-subtree=children' -o xml   > /tmp/collect-apic-$HOSTNAME-p32-vmmDomP-children.xml

echo "Get vmmUsrAggr [33/44]..."
moquery -c vmmUsrAggr                                  > /tmp/collect-apic-$HOSTNAME-p33-vmmUsrAggr.txt
moquery -c vmmUsrAggr -o table                         > /tmp/collect-apic-$HOSTNAME-p33-vmmUsrAggr-table.txt
moquery -c vmmUsrAggr -o xml                           > /tmp/collect-apic-$HOSTNAME-p33-vmmUsrAggr.xml
moquery -c vmmUsrAggr -x 'rsp-subtree=children'        > /tmp/collect-apic-$HOSTNAME-p33-vmmUsrAggr-children.txt
moquery -c vmmUsrAggr -x 'rsp-subtree=children' -o xml > /tmp/collect-apic-$HOSTNAME-p33-vmmUsrAggr-children.xml

echo "Get compCtrlr [34/44]..."
moquery -c compCtrlr                                   > /tmp/collect-apic-$HOSTNAME-p34-compCtrlr.txt
moquery -c compCtrlr -o table                          > /tmp/collect-apic-$HOSTNAME-p34-compCtrlr-table.txt
moquery -c compCtrlr -o xml                            > /tmp/collect-apic-$HOSTNAME-p34-compCtrlr.xml
moquery -c compCtrlr -x 'rsp-subtree=children'         > /tmp/collect-apic-$HOSTNAME-p34-compCtrlr-children.txt
moquery -c compCtrlr -x 'rsp-subtree=children' -o xml  > /tmp/collect-apic-$HOSTNAME-p34-compCtrlr-children.xml

echo "Get VMM hypervisor (compHv) [35/44]..."
moquery -c compHv                                      > /tmp/collect-apic-$HOSTNAME-p35-compHv.txt
moquery -c compHv -o table                             > /tmp/collect-apic-$HOSTNAME-p35-compHv-table.txt
moquery -c compHv -o xml                               > /tmp/collect-apic-$HOSTNAME-p35-compHv.xml
moquery -c compHv -x 'rsp-subtree=children'            > /tmp/collect-apic-$HOSTNAME-p35-compHv-children.txt
moquery -c compHv -x 'rsp-subtree=children' -o xml     > /tmp/collect-apic-$HOSTNAME-p35-compHv-children.xml

echo "Get VLAN Pool (fvnsVlanInstP) [36/44]..."
moquery -c fvnsVlanInstP                                    > /tmp/collect-apic-$HOSTNAME-p36-fvnsVlanInstP.txt
moquery -c fvnsVlanInstP -o table                           > /tmp/collect-apic-$HOSTNAME-p36-fvnsVlanInstP-table.txt
moquery -c fvnsVlanInstP -o xml                             > /tmp/collect-apic-$HOSTNAME-p36-fvnsVlanInstP.xml
moquery -c fvnsVlanInstP -x 'rsp-subtree=children'          > /tmp/collect-apic-$HOSTNAME-p36-fvnsVlanInstP-children.txt
moquery -c fvnsVlanInstP -x 'rsp-subtree=children' -o xml   > /tmp/collect-apic-$HOSTNAME-p36-fvnsVlanInstP-children.xml

echo "Get L2 Policy (l2IfPol) [37/44]..."
moquery -c l2IfPol                                          > /tmp/collect-apic-$HOSTNAME-p37-l2IfPol.txt
moquery -c l2IfPol -o table                                 > /tmp/collect-apic-$HOSTNAME-p37-l2IfPol-table.txt
moquery -c l2IfPol -o xml                                   > /tmp/collect-apic-$HOSTNAME-p37-l2IfPol.xml
moquery -c l2IfPol -x 'rsp-subtree=children'                > /tmp/collect-apic-$HOSTNAME-p37-l2IfPol-children.txt
moquery -c l2IfPol -x 'rsp-subtree=children' -o xml         > /tmp/collect-apic-$HOSTNAME-p37-l2IfPol-children.xml

echo "Get Interface Policy Group (infraAccPortGrp) [38/44]..."
moquery -c infraAccPortGrp                                  > /tmp/collect-apic-$HOSTNAME-p38-infraAccPortGrp.txt
moquery -c infraAccPortGrp -o table                         > /tmp/collect-apic-$HOSTNAME-p38-infraAccPortGrp-table.txt
moquery -c infraAccPortGrp -o xml                           > /tmp/collect-apic-$HOSTNAME-p38-infraAccPortGrp.xml
moquery -c infraAccPortGrp -x 'rsp-subtree=children'        > /tmp/collect-apic-$HOSTNAME-p38-infraAccPortGrp-children.txt
moquery -c infraAccPortGrp -x 'rsp-subtree=children' -o xml > /tmp/collect-apic-$HOSTNAME-p38-infraAccPortGrp-children.xml
 
echo "Get PC/VPC Policy Group (infraAccBndlGrp) [39/44]..."
moquery -c infraAccBndlGrp                                  > /tmp/collect-apic-$HOSTNAME-p39-infraAccBndlGrp.txt
moquery -c infraAccBndlGrp -o table                         > /tmp/collect-apic-$HOSTNAME-p39-infraAccBndlGrp-table.txt
moquery -c infraAccBndlGrp -o xml                           > /tmp/collect-apic-$HOSTNAME-p39-infraAccBndlGrp.xml
moquery -c infraAccBndlGrp -x 'rsp-subtree=children'        > /tmp/collect-apic-$HOSTNAME-p39-infraAccBndlGrp-children.txt
moquery -c infraAccBndlGrp -x 'rsp-subtree=children' -o xml > /tmp/collect-apic-$HOSTNAME-p39-infraAccBndlGrp-children.xml

echo "Get Associate interface and policies (infraAccPortP) [40/44]..."
moquery -c infraAccPortP                                    > /tmp/collect-apic-$HOSTNAME-p40-infraAccPortP.txt
moquery -c infraAccPortP -o table                           > /tmp/collect-apic-$HOSTNAME-p40-infraAccPortP-table.txt
moquery -c infraAccPortP -o xml                             > /tmp/collect-apic-$HOSTNAME-p40-infraAccPortP.xml
moquery -c infraAccPortP -x 'rsp-subtree=children'          > /tmp/collect-apic-$HOSTNAME-p40-infraAccPortP-children.txt
moquery -c infraAccPortP -x 'rsp-subtree=children' -o xml   > /tmp/collect-apic-$HOSTNAME-p40-infraAccPortP-children.xml

echo "Get infraPortBlk [41/44]..."
moquery -c infraPortBlk                                     > /tmp/collect-apic-$HOSTNAME-p41-infraPortBlk.txt
moquery -c infraPortBlk -o table                            > /tmp/collect-apic-$HOSTNAME-p41-infraPortBlk-table.txt
moquery -c infraPortBlk -o xml                              > /tmp/collect-apic-$HOSTNAME-p41-infraPortBlk.xml
moquery -c infraPortBlk -x 'rsp-subtree=children'           > /tmp/collect-apic-$HOSTNAME-p41-infraPortBlk-children.txt
moquery -c infraPortBlk -x 'rsp-subtree=children' -o xml    > /tmp/collect-apic-$HOSTNAME-p41-infraPortBlk-children.xml

echo "List all Interface Profile associate with policy group (infraRsAccBaseGrp) [42/44]..."
moquery -c infraRsAccBaseGrp                                     > /tmp/collect-apic-$HOSTNAME-p42-infraRsAccBaseGrp.txt
moquery -c infraRsAccBaseGrp -o table                            > /tmp/collect-apic-$HOSTNAME-p42-infraRsAccBaseGrp-table.txt
moquery -c infraRsAccBaseGrp -o xml                              > /tmp/collect-apic-$HOSTNAME-p42-infraRsAccBaseGrp.xml
moquery -c infraRsAccBaseGrp -x 'rsp-subtree=children'           > /tmp/collect-apic-$HOSTNAME-p42-infraRsAccBaseGrp-children.txt
moquery -c infraRsAccBaseGrp -x 'rsp-subtree=children' -o xml    > /tmp/collect-apic-$HOSTNAME-p42-infraRsAccBaseGrp-children.xml

echo "Get List all Static Port (fvRsPathAtt) [43/44]..."
moquery -c fvRsPathAtt                                     > /tmp/collect-apic-$HOSTNAME-p43-fvRsPathAtt.txt
moquery -c fvRsPathAtt -o table                            > /tmp/collect-apic-$HOSTNAME-p43-fvRsPathAtt-table.txt
moquery -c fvRsPathAtt -o xml                              > /tmp/collect-apic-$HOSTNAME-p43-fvRsPathAtt.xml
moquery -c fvRsPathAtt -x 'rsp-subtree=children'           > /tmp/collect-apic-$HOSTNAME-p43-fvRsPathAtt-children.txt
moquery -c fvRsPathAtt -x 'rsp-subtree=children' -o xml    > /tmp/collect-apic-$HOSTNAME-p43-fvRsPathAtt-children.xml

echo "Get all Active Faults [44/44] <1/4> ."
moquery -c faultInst                                      > /tmp/collect-apic-$HOSTNAME-p44-faultInst.txt
echo "Get all Active Faults [44/44] <2/4> .."
moquery -c faultInst | egrep -e "^code" | sort | uniq -c  > /tmp/collect-apic-$HOSTNAME-p44-faultInst-code.txt
echo "Get all Active Faults [44/44] <3/4> ..."
moquery -c faultInst | egrep -e "^descr" | sort | uniq -c > /tmp/collect-apic-$HOSTNAME-p44-faultInst-descr.txt
echo "Get all Active Faults [44/44] <4/4> ...."
moquery -c faultInst -o xml                               > /tmp/collect-apic-$HOSTNAME-p44-faultInst.xml

FILETARGZ=/tmp/collect-apic-$HOSTNAME-$DATA.tar.gz
tar -czf $FILETARGZ ./collect-apic-*$HOSTNAME-p*

echo
echo "FINISHED"
echo "-------------------"
echo "Please copy file $FILETARGZ"
echo "note: If use Winscp select binary file transfer file."
echo

#EOF
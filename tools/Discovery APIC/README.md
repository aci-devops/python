# COMO ENCONTRAR OS PARAMETROS DE CONFIGURAÇÃO FORAM UTILIZADOS DURANTE A SETUP DO APIC 01?

Provavelmente você já se deparou nessa situação! Um colega de trabalho configurou o APIC 01 e por algum motivo não te passou os parametros que foram configurados.
Seus problemas acabaram!


## Vamos ao "PULO DO GATO":

## NOME DO FABRIC:
````
moquery -c infraCont | grep -E "dn|fbDmNm|size"
````
````
lab1-fab1-ntt-apic1# moquery -c infraCont | grep -E "dn|fbDmNm|size"
dn : topology/pod-1/node-1/av
fbDmNm : lab1-fab1-ntt
size : 3
dn : topology/pod-2/node-2/av
fbDmNm : lab1-fab1-ntt
size : 3
dn : topology/pod-1/node-3/av
fbDmNm : lab1-fab1-ntt
size : 3
````

## NÚMERO DE CONTROLADORAS DO CLUSTER:
````
moquery -c infraClusterPol | grep "size"
````
````
lab1-fab1-ntt-apic1# moquery -c infraClusterPol | grep "size"
size : 3
````

## ID DA CONTROLADORA:
````
show controller detail | grep -E -B 1 -A 1 "Name"
````

````
lab1-fab1-ntt-apic1# show controller detail | grep -E -B 1 -A 1 "Name"
ID : 1*
Name : lab1-fab1-ntt-apic1
UUID : eb9d0b1c-322b-11e6-ac7c-153c2ef5dee4

ID : 2
Name : lab1-fab1-ntt-apic2
UUID : 4ebcd446-3230-1126-a0a8-8d8b58d121218f

ID : 3
Name : lab1-fab1-ntt-apic3
UUID : 0f3fb24a-3231-2121-b132-fde02bffdbc5
```
````
## ENDEREÇAMENTO IP PARA O TEP:
````
moquery -c fabricSetupP | grep -E “podId|tepPool"
````
````
lab1-fab1-ntt-apic1# moquery -c fabricSetupP | grep -E "podId|tepPool"
podId : 1
tepPool : 10.0.0.0/16
podId : 2
tepPool : 10.1.0.0/16
````
````
````
## ENDEREÇAMENTO MULTICAST (GIPO)
````
moquery -c fvBD | grep -E "name|bcastP|dn" | grep -B 2 "infra"
````
````
lab1-fab1-ntt-apic1# moquery -c fvBD | grep -E "name|bcastP|dn" | grep -B 2
"infra"
name : default
bcastP : 225.0.0.16
dn : uni/tn-infra/BD-default
````
## SPEED DA INTERFACE DE GERÊNCIA:
````
cat /proc/net/bonding/bond1
ethtool eth1-1 | grep "-negotiation"
ethtool eth1-2 | grep "-negotiation"
````
````
lab1-fab1-ntt-apic1# cat /proc/net/bonding/bond1
Ethernet Channel Bonding Driver: v3.7.1 (April 27, 2011)
Bonding Mode: fault-tolerance (active-backup)
Primary Slave: None
Currently Active Slave: eth1-1
MII Status: up
MII Polling Interval (ms): 60
Up Delay (ms): 0
Down Delay (ms): 0
Slave Interface: eth1-1
MII Status: up
Speed: 1000 Mbps
Duplex: full
Link Failure Count: 0
Permanent HW addr: 24:e9:b3:15:a0:ee
Slave queue ID: 0
Slave Interface: eth1-2
MII Status: down
Speed: Unknown
Duplex: Unknown
Link Failure Count: 0
Permanent HW addr: 24:e9:b3:15:a0:ef
Slave queue ID: 0
````
````
lab1-fab1-ntt-apic1# ethtool eth1-1 | grep "-negotiation"
6: Supports auto-negotiation: Yes
11: Advertised auto-negotiation: Yes
17: Auto-negotiation: on
````
````
lab1-fab1-ntt-apic1# ethtool eth1-2 | grep "-negotiation"
6: Supports auto-negotiation: Yes
11: Advertised auto-negotiation: Yes
17: Auto-negotiation: on
````

## VLAN DE INFRA:
````
ifconfig | grep "bond0."
````
````

lab1-fab1-ntt-apic1# ifconfig | grep "bond0."
bond0 Link encap:Ethernet HWaddr 90:E2:BA:4B:FC:78
bond0.4094 Link encap:Ethernet HWaddr 90:E2:BA:4B:FC:78
````

## IPV4/IPV6 DA OOB:
````
show controller detail id 1 | grep "OOB"
````
````
lab1-fab1-ntt-apic1# show controller detail id 1 | grep "OOB"
OOB IPv4 Address : 10.122.254.211
OOB IPv6 Address : 2002:10:122:254::d3
````

## DEFAULT GATEWAY:
````
netstat -A inet -rn | grep "oobmgmt"
````
````
lab1-fab1-ntt-apic1# netstat -A inet -rn | grep "oobmgmt"
0.0.0.0 10.122.254.1 0.0.0.0 UG 0 oobmgmt
10.122.254.0 0.0.0.0 255.255.255.0 U 0 oobmgmt
````

## IP DO TEP PARA O APIC:
````
ifconfig -a bond0.4094 | grep "inet"
````

````
lab1-fab1-ntt-apic1# ifconfig -a bond0.4094 | grep "inet"
inet addr:10.0.0.1 Bcast:10.0.0.1 Mask:255.255.255.255
inet6 addr: fe80::92e2:baff:fe4b:fc78/64 Scope:Link
````

## DEFAULT GATEWAY DO TEP
````
netstat -A inet -rn | grep bond0.4094
````

````
lab1-fab1-ntt-apic1# netstat -A inet -rn | grep bond0.4094
10.0.0.0 10.0.0.30 255.255.0.0 UG 0 0 bond0.4094
10.0.0.30 0.0.0.0 255.255.255.255 UH 0 0 bond0.4094
10.0.104.64 10.0.0.30 255.255.255.255 UGH 0 0 bond0.4094
10.0.104.66 10.0.0.30 255.255.255.255 UGH 0 0 bond0.4094
````


## VERIFICANDO SE O STRONG PASSWORD FOI SETADO:
````
moquery -c aaaUserEp | grep "pwdStrengthCheck"
````

````
lab1-fab1-ntt-apic1# moquery -c aaaUserEp | grep "pwdStrengthCheck"
pwdStrengthCheck : no
````


Pronto! Agora você já tem todas as informações em mãos! Bom trabalho.

## Autor
Freitas®
